const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
var views = require('koa-views');

const PORT = 2233
const home = require('./router')
const user = require('./router/user')

const render = views(__dirname + '/public', {
	extension: 'ejs'
})

const app = new Koa()
app.use(bodyParser())
app.use(render)

app.use(async (ctx, next) => {
	ctx.set('Access-Control-Allow-Origin', '*');
	console.log(`Method：${ctx.request.method}    Url：${ctx.request.url}`);
	await next()
	if (parseInt(ctx.status) === 404) {
		ctx.response.redirect('/404')
	}
})


app.use(home.routes())
app.use(user.routes())

app.listen(PORT)

console.log(`  
Server running at:
- Local:   http://localhost:${PORT}
- Network: http://10.10.10.42:${PORT}
`);
