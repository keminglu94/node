const router = require('koa-router')()

router.get('/', async (ctx, next) => {
	ctx.response.body = `<h1>hello nodejs</h1>`
})

router.get('/404', async (ctx, next) => {
	ctx.response.body = `<h1>this page not find</h1>`
})

module.exports = router