const router = require('koa-router')()
const UserModel = require('../dbs/models/user.js')
const { query, save, updateOne, deleteOne } = require('../dbs/sql.js')
let formatData = require('../common/dataFormat.js')

router.get('/user/list', async (ctx, next) => {
	let userList = await query(UserModel)
	console.log(userList);
	// 直接把数据暴露出去，外部就能拿到。
	ctx.response.body = { ...formatData, data: userList }
	// await ctx.render('userList', { userList })
})

router.get('/user/form', async (ctx, next) => {
	await ctx.render('addUser', {})
})

router.get('/user/del', async (ctx, next) => {
	ctx.response.body = `<h1>del page</h1>`
})

// 使用 postman 测试post接口时 需要选择 body 下面的 x-www-form-urlencoded，因为该接口就是拿到了form表单提交的数据。
router.post('/user/del', async (ctx, next) => {
	let { username } = ctx.request.body
	console.log(username);
	if (!username) {
		ctx.response.body = `<h1>del fail</h1><div>请使用form表单传递数据 或者 使用x-www-form-urlencoded</div>`
		return
	}
	try {
		let res = await deleteOne(UserModel, { username })
		// ctx.response.body = `<h1>del ${username} success</h1><div>${res}</div>`
		ctx.response.body = { ...formatData, data: res }
	} catch (error) {
		// ctx.response.body = `<h1>del fail</h1><div>${error}</div>`
		ctx.response.body = { ...formatData, data: error }
	}
})

router.post('/user/add', async (ctx, next) => {
	let { username, password, name, age, sex, tel } = ctx.request.body
	let user = new UserModel({
		username,
		password,
		name,
		age,
		sex,
		tel
	})
	// 如果想要拿到数据检查的报错，用 try catch，不然拿不到错误，从而不能给客户端提示
	// await 只能拿到promise的resolve数据
	try {
		let res = await save(user)
		ctx.response.body = `<h1>add success</h1><div>${res}</div>`
	} catch (error) {
		ctx.response.body = `<h1>add fail</h1><div>${error}</div>`
	}
})

router.post('/user/update', async (ctx, next) => {
	let { username, name, age, tel } = ctx.request.body
	let query = {
		username
	}
	let updateData = {
		name, age, tel,
	}
	console.log(ctx.request.body);
	try {
		let res = await updateOne(UserModel, query, updateData)
		ctx.response.body = `<h1>update user success</h1>`
	} catch (error) {
		ctx.response.body = `<h1>update user fail</h1><div>${error}</div>`
	}
})

module.exports = router