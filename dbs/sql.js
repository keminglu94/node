const save = model => {
	return new Promise((resolve, reject) => {
		model.save((err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

const query = (model, query) => {
	return new Promise((resolve, reject) => {
		model.find(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

const hasOne = (model, query) => {
	return new Promise((resolve, reject) => {
		model.findOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

const deleteOne = (model, query) => {
	return new Promise((resolve, reject) => {
		model.deleteOne(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

const deleteMany = (model, query) => {
	return new Promise((resolve, reject) => {
		model.deleteMany(query, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

const updateOne = (model, query, updateData) => {
	return new Promise((resolve, reject) => {
		model.findOneAndUpdate(query, updateData, (err, data) => {
			if (err) {
				reject(err)
			}
			resolve(data)
		})
	})
}

module.exports = {
	save,
	query,
	hasOne,
	deleteOne,
	deleteMany,
	updateOne,
}


