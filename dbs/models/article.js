var mongoose = require('../index.js');
var Schema = mongoose.Schema;

var ArticleSchema = new Schema({
	title: {
		type: String,
		unique: true
	},
	cid: {
		//分类id
		type: Schema.Types.ObjectId,
		ref: "ArticleCate"    //cid和 文章分类建立关系。   model    
	},
	author_id: {
		//用户id
		type: Schema.Types.ObjectId,
		ref: "User"    //author_id和 用户表建立关系。   model
	},
	author_name: String,
	description: String,
	content: String
});

module.exports = mongoose.model('Article', ArticleSchema, 'article');