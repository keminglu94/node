var mongoose = require('../index.js');

let newsSchema = {
	title: {
		type: String,
		trim: true, // 去掉两边空格
	},
	author: String,
	pic: String,
	content: String,
	status: {
		type: Number,
		default: 1
	}
}

module.exports = mongoose.model('News', newsSchema, 'news')

