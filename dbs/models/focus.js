var mongoose = require('../index.js');

let focusSchema = {
	title: {
		type: String,
		trim: true, // 去掉两边空格
	},
	pic: String,
	redirect: {
		type: String,
		set(value) {
			// 增加数据时对 redirect 这个字段进行处理
			// value 可以获取 redirect 的值，返回的数据就是 redirect 在数据库实际保存的值
			// 功能 www.baidu.com --->  https://www.baidu.com
			if (!value) {
				return ''
			} else {
				if(!value.startsWith('https://')){
					return 'https://' + value
				}
				return value
			}
		}
	},
	status: {
		type: Number,
		default: 1
	}
}

module.exports = mongoose.model('Focus', focusSchema, 'focus')

