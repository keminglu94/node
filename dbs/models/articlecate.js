var mongoose = require('../index.js');

var ArticleCateSchema = new mongoose.Schema({
	title: {
		type: String,
		unique: true
	},
	description: String,
	addTime: Date
});

module.exports = mongoose.model('ArticleCate', ArticleCateSchema, 'articlecate');