var mongoose = require('../index.js');

//数据校验
/**
 * 	required 表示这个数据必须传入
 * 	max 最大值
 * 	min 最小值
 * 	enum 枚举类型，要求数据必须满足枚举值
 * 	match 增加的数据必须符合 match（正则）的规则
 * 	maxlength 最大长度
 * 	minlength 最小长度
 *  validate 自定义校验，返回bool类型
 */

var UserSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		validate: function (value) {
			return value.length > 1
		}
	},
	password: {
		type: String,
		minlength: 6,
	},
	name: String,
	age: Number,
	sex: String,
	tel: Number,
	status: {
		type: Number,
		default: 1
	}
});

// 第三个参数才是对应的集合，如果不设置则对应的集合是 第一个参数+‘s’，即 users
module.exports = mongoose.model('User', UserSchema, 'user');