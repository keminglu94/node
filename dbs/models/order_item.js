var mongoose = require('../index.js');

let orderItemSchema = mongoose.Schema({
	order_id: String,
	title: String,
	price: Number,
	num: Number,
})

module.exports = mongoose.model('OrderItem', orderItemSchema, 'order_item')

